import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import {applyMiddleware, createStore} from 'redux';
import { Provider } from 'react-redux';
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from 'redux-thunk';
import './style/index.css';
import './style/narrow-jumbotron.css'
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import combineReducers from "./reducers/index";
import 'bootstrap/dist/css/bootstrap.min.css';

const store = createStore(combineReducers, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
