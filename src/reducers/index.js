import { combineReducers } from 'redux';
import tracks from './tracks.js';
import playlists from "./playlists";
import filterTracks from "./filterTracks";


export default combineReducers({
    tracks,
    playlists,
    filterTracks
})