import React, {Component} from 'react';
import {FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {getTracks} from "../actions/tracks";

class TracksForm extends Component {
    render() {
        return (
            <form>
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder"/>

                    <button onClick={this.state.onGetTracks}>Get Tracks</button>

                    <input type="text" ref={(input) => {
                        this.state.trackInput = input;
                    }}/>
                    <button onClick={this.state.addTrack}>Add track</button>

                    <input type="text" ref={(input) => {
                        this.state.searchInput = input;
                    }}/>
                    <button onClick={this.state.findTrack}>Find track</button>

                </FormGroup>
            </form>
        );
    }
}

export default TracksForm;


