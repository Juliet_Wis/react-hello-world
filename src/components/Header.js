import React from 'react';
import Menu from "./Menu";

const Header = () => (
    <div className="header clearfix">
        <Menu />
        <h1 className="text-muted">Track list</h1>
    </div>
);

export default Header;