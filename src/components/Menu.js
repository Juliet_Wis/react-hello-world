import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () => (
    <nav>
        <ul className="nav nav-pills float-right">
            <li className="nav-item">
                <Link to="/" className="nav-link active">Tracks<span className="sr-only">(current)</span></Link>
            </li>
            <li className="nav-item">
                <Link to="/about" className="nav-link">About</Link>
            </li>
        </ul>
    </nav>
);

export default Menu;