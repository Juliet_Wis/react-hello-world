import React, { Component } from 'react';
import Header from "./Header";
import Footer from "./Footer";

class  MainLayout extends Component {
    render() {
        return(
            <div className="container">
                <Header/>
                <div>
                    {this.props.children}
                </div>
                <Footer/>
            </div>
        );
    };
}

export default MainLayout;