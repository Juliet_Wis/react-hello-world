import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";
import MainLayout from "./MainLayout";
import Home from '../containers/Home';
import About from "./About";

class App extends Component{

    render(){
        return (
            <MainLayout>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/about" component={About}/>
                </Switch>
            </MainLayout>
        );
    }
}

export default App;
