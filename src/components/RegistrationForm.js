import React, {Component} from 'react';
import '../style/RegistrationForm.css';

class RegistrationForm extends Component{
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.state = {
            email: ''
        };
    }

    handleSubmit(event){
        event.preventDefault();
        console.log('form submitted and email value is', this.state.email);
    }

    handleEmailChange(event){
        console.log('change email', this);
        this.setState({email: event.target.value});
    }

    render(){
        return(
            <form onSubmit={this.handleSubmit}>
                <input
                    type="text"
                    placeholder="E-mail"
                    value={this.state.email}
                    onChange={this.handleEmailChange}
                    className="emailField"
                />
                <button className="submitBtn">Submit</button>
            </form>
        );
    };
}

export default RegistrationForm;