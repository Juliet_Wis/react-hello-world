import React, { Component } from 'react';
import {connect} from "react-redux";
import {getTracks} from "../actions/tracks";
import {Button, Col, InputGroup, ListGroup, ListGroupItem, Row} from "reactstrap";

class Home extends Component{
    addTrack = () => {
        this.props.onAddTrack(this.trackInput.value);
        this.trackInput.value = '';
    };

    findTrack = () => {
        this.props.onFindTrack(this.searchInput.value);
        console.log('find track', this.searchInput.value);
    };
    render(){
        return(
            <Row className="jumbotron">
                <Col xs="4">
                    <InputGroup>
                        <input type="text" ref={(input) => { this.trackInput = input; }} className="form-control"/>
                        <Button onClick={this.addTrack.bind(this)} color="primary">Add  track</Button>
                    </InputGroup>
                    <InputGroup>
                        <input type="text" ref={(input) => { this.searchInput = input; }} className="form-control"/>
                        <Button onClick={this.findTrack.bind(this)}>Find track</Button>
                    </InputGroup>
                </Col>
                <Col xs="8">
                    <InputGroup>
                        <Button onClick={this.props.onGetTracks} color="secondary">Get Tracks</Button>
                    </InputGroup>
                    <ListGroup>
                        {this.props.tracks.map((track, index) =>
                            <ListGroupItem key={index}>{track.name}</ListGroupItem>
                        )}
                    </ListGroup>
                </Col>
            </Row>
        );
    };
}

export default connect(
    state => ({
        tracks: state.tracks.filter( track => track.name.includes(state.filterTracks) )
    }),
    dispatch => ({
        onAddTrack: (name) => {
            const payload = {
                id: Date.now().toString(),
                name
            };
            dispatch({ type: 'ADD_TRACK', payload })
        },
        onFindTrack: (name) => {
            dispatch({ type: 'FIND_TRACK', payload: name })
        },
        onGetTracks: () => {
            dispatch(getTracks());
        }
    })
)(Home);